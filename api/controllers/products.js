const Product = require('../models/product');

// This function is written to fetch all the product
//=========================================================
exports.fetchAllProduct = (req, res, next) => {

	Product.find()
    .select('name price _id productImage')
    .exec()
    .then(result => {
        console.log(result);

       
        if (result.length>0) {
            let response = {
                message: "product fetched",
                nop: result.length,
                product: result.map(result => {
                    return {
                        name: result.name,
                        price: result.price,
                        _id: result._id,
                        productImage: result.productImage,
                        request: 'GET'
                    }
                })
            };

            res.status(200).json(response);
        } else {
            res.status(404).json({
             message: "No product there",
              
            });
        }
        
    })
    .catch(err => {
        console.log(err);
        res.json({
           message: "product was not fetched",
           error: err 
        });
    });
}
//=====================================================================

// This function is written to fetch all the product
//=========================================================
exports.fetchProduct = (req, res, next) => {
    var id = req.body.id; 
    console.log(req.body);
    
    Product.findById(id)
    .exec()
    .then(result => {
        console.log(result);
        if (result) {
            res.status(200).json({
             message: "product fetched",
             product: result 
            });
        } else {
            res.status(404).json({
             message: "No product there",
              
            });
        }
        
    })
    .catch(err => {
        console.log(err);
        res.json({
           message: "product was not fetched",
           error: err 
        });
    });
    
}
//=====================================================================

