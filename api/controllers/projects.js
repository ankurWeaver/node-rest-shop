const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');
const Project = require('../models/project');

// This function is written to fetch all the product
//=========================================================
exports.fetchAllProject = (req, res, next) => {
    

	Project.find()
    .select('projectId projectName teamSize')
    .exec()
    .then(result => {
        
        if (result.length>0) {
            res.json({
                result:result
            })
        } else {
            res.status(404).json({
               message: "No project there",          
            });
        }
        
    })
    .catch(err => {
        console.log(err);
        res.json({
           message: "project was not fetched",
           error: err 
        });
    });
}
//=====================================================================


// This function is written to fetch all the product
//=========================================================
exports.addProject = (req, res, next) => {
   
    const project = new Project({
        _id: new mongoose.Types.ObjectId(),
        projectId: req.body.projectId,
        projectName: req.body.projectName,
        teamSize: req.body.teamSize     
    });
    

    project.save()
    .then(result => {
        res.status(200).json({
            result: result
        });
    }).catch(err => {
        res.status(200).json({
            error : err
        });
    });
   

}
//=====================================================================

