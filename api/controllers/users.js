const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Order = require('../models/order');
const Product = require('../models/product');
const Project = require('../models/project');
const User = require('../models/user');

const fetchAllUser = (req, res, next) => {
   
    User.find({
        // email: email
    })
    .then(user => {
        return res.status(200).json({
            result: user
        });
    })
    .catch(err => {
        return res.status(500).json({
            error: err 
        });
    });

} // END const fetchAllUser = (req, res, next) => {


let signUp = (req, res, next) => {  // start Sign up
    User.find({
        email: req.body.email
    })
    // .exec()
    .then(user =>{
        if (user.length > 0) {
            return res.status(409).json({
                message: 'Mail Exists' 
            });
        } else {
    
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err 
                    });
                } else {
                    var user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        email: req.body.email,
                        password: hash
                    });

                    user.save()
                        .then(result => {
                                res.json({
                                    message: "user created",
                                    product: result
                                });
                        })
                        .catch(err => {
                            return res.status(500).json({
                                error: err 
                            });
                        });
                }
            }); 
        }
    })	
} // start Sign up

const userLogin = (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    
    User.find({
        email: email
    })
    .then(user => {
        
        if (user.length < 1) {
            return res.status(401).json({
                message:'Auth Failed'
            }); 
        }

        if (user.length > 0) {
            
            bcrypt.compare(password, user[0].password, (err, result) => {
                if (err) {
                    return res.status(402).json({
                        message:'error occur'
                    });
                } 

                
                if (result == true) {
                    const token = jwt.sign(
                        {
                          email: user[0].email,
                          userId: user[0]._id
                        }, 
                        'secret', 
                        {
                            expiresIn: '12h' 
                        }
                    );
                    return res.status(200).json({
                        message:'Auth Successfull',
                        token: token
                    }); 
                } else {
                    return res.status(401).json({
                        message:'Auth Failed'
                    }); 
                }    
               
            });
            
        }
    })
    .catch(err => {
        return res.status(401).json({
            error: err 
        }); 
    });

}

const deleteUser = (req, res, next) => {
    let email = req.body.email;
    User.remove({
       email: email
    })
    .then(result => {
         return res.status(200).json({
            message: 'user deleted'
        });
    })
    .catch(err => {
        return res.status(500).json({
            error: err 
        });
    });

} // END const deleteUser = (req, res, next) => {


module.exports = {
    fetchAllUser: fetchAllUser,
    signUp: signUp,
    userLogin: userLogin,
    deleteUser: deleteUser
};