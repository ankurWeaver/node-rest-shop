const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {

	try {
		
		let token = req.headers.authorisation.split(" ")[1];
        let decoded = jwt.verify(token, 'secret');
       
        req.userdata = decoded;
        next();

	} catch (error) {
		return res.status(401).json({
			message: 'Auth Failed'
		});
	} 

};