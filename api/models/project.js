const mongoose = require('mongoose');

const projectSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	projectId: {
		type: String,
		default: ''
	},
	projectName: {
		type: String,
		default: ''
	},
	teamSize: {
		type: Number,
		default: 1
	},
});

module.exports = mongoose.model('Project', projectSchema);