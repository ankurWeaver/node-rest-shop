const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const Order = require('../models/order');
const Product = require('../models/product');

router.get('/', (req, res, next) => {
    res.status(200).json({
    	message: "get method in order routes"
    });
});

router.post('/add-order', (req, res, next) => {

	var id = req.body.productId;
    console.log(req.body);
    
    Product.findById(id)
    .exec()
    .then(result => {
        
        if (result) {
            
            const order = new Order({
				_id: new mongoose.Types.ObjectId(),
				quantity: req.body.quantity,
				product: req.body.productId
			});

			order.save()
			.then(result => {
				res.status(200).json({
			    	result: result
			    });
			}).catch(err => {
				res.status(200).json({
			    	error : err
			    });
			});

        } else {
            res.status(404).json({
             message: "No product there",
              
            });
        }
        
    })
    .catch(err => {
        console.log(err);
        res.json({
           message: "product was not fetched",
           error: err 
        });
    });

    return;

	

	
	// 
    
});


router.post('/fetch-all-order', (req, res, next) => {

	Order.find()
	.select('product quantity _id')
	.populate('product', 'name price')
	.exec()
	.then(result => {
		
		res.status(200).json({
			count: result.length,
	    	order: result.map(result => {
	    		return {
	    			_id: result._id,
	    			quantity: result.quantity,
	    			product: result.product
	    		}
	    	})
	    });
	})
	.catch(err => {
		
		res.status(200).json({
	    	error : err
	    });
	});
    
});



module.exports = router;