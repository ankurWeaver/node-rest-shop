const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');

const Product = require('../models/product');
const checkAuth = require('../middleware/check-auth');

const OrderControllers = require('../controllers/products');

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './uploads/');
    },

    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg') {
        cb(null, true);
    } else {
        cb(null, false);
    }     
};

const upload = multer({
    // dest: 'uploads/'
    storage: storage,
    fileFilter: fileFilter,
    limits: {fileSize: 1024 * 1024 * 5}
});

// This funxtion is written to store files
router.post('/uploadProduct', upload.single('productImage'), (req, res, next) => {
    
   
   var product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
        productImage: req.file.path  
    });

    product.save()
    .then(result => {
        console.log(result);
        res.json({
         message: "product added",
         product: {
            name: result.name,
            price: result.price,
            _id: result._id,
            productImage: result.productImage,
            request: 'GET'
         }
        });
    })
    .catch(err => console.log(err));    


});

router.get('/', (req, res, next) => {
    res.status(200).json({
    	message: "get method in routes",
    	name: req.body.name
    });
});

router.post('/addProduct',checkAuth, (req, res, next) => {
    console.log(req.userdata);
    let product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price 
    });
    console.log(product);

    product.save()
    .then(result => {
        console.log(result);
        res.json({
         message: "product added",
         product: {
            name: result.name,
            price: result.price,
            _id: result._id,
            requestCredentials: req.userdata
         }
        });
    })
    .catch(err => console.log(err));    
});

router.post('/fetchProduct', OrderControllers.fetchProduct);

router.post('/fetchALLProduct', OrderControllers.fetchAllProduct);



router.delete('/:prductId', (req, res, next) => {

    var id = req.params.prductId;

    Product.remove({_id: id})
    .exec()
    .then(result => {
        res.status(200).json({
            message: "product Deleted",
            product: result 
        });
    })
    .catch(err => {
        console.log(err);
        res.json({
           message: "product was not fetched",
           error: err 
        });
    });

});


router.post('/update-product', (req, res, next) => {

    var id = req.body.id;
    let updateOps = {};

    for(let ops of req.body.parameters) {
        updateOps[ops.propName] = ops.value;
    }

    Product.update(
       {_id: id},
       { $set: updateOps})
    .then(result => {
        res.status(200).json({
            msg: "Updated",
            result: result
        });
    })
    .catch(err => {
         res.status(200).json({
            msg: "Not Updated",
            error: err
        });
    });     
    
});







// router.get('/:ProductId', (req, res, next) => {
//     res.status(200).json({
//     	message: req.params.ProductId
//     });
// });

module.exports = router;