const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');

const Product = require('../models/product');
const Project = require('../models/project');
const checkAuth = require('../middleware/check-auth');

const OrderControllers = require('../controllers/products');
const ProjectControllers = require('../controllers/projects');


router.get('/fetch-all-project', ProjectControllers.fetchAllProject);
router.post('/add-project', ProjectControllers.addProject);


module.exports = router;