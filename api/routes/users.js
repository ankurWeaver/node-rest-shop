const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const UserControllers = require('../controllers/users');

// This function is used to register the user in the system
router.post('/signup', UserControllers.signUp); // End Sign up
// This funxtion is user to fetch all the users
router.post('/fetch-all-user', UserControllers.fetchAllUser);

// this function is used to delete the user on the basis of email
router.post('/delete-user', UserControllers.deleteUser);

// This fuction is used to make successfull login
router.post('/login',  UserControllers.userLogin);


module.exports = router;