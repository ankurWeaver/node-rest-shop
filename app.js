const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const productRoutes = require('./api/routes/products.js');
const orderRoutes = require('./api/routes/orders.js');
const userRoutes = require('./api/routes/users.js');
const projectRoutes = require('./api/routes/projects.js');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());



// establishing databse connection
var db = 'mongodb://localhost/ganguly_tech';
var dbname = "ganguly_tech";
mongoose.connect(db, function () {
	console.log("Database connected "+dbname);
});


// keep the log of the url hitted
app.use(morgan('dev'));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin","*");
	res.header("Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization");

	if (req.method === 'OPTIONS' ) {
        res.header("Access-Control-Allow-Methods","PUT, POST, GET, PATCH, DELETE");
        return res.status(200).json({});
	}
	next(); 
});

app.use('/products',productRoutes);
app.use('/orders',orderRoutes);
app.use('/users',userRoutes);
app.use('/projects',projectRoutes);

app.use('/uploads',express.static('uploads'));


app.use((req, res, next) => {
	const error = new Error('Not found');
	error.status=404;
	res.json({
    	message: error.message
    });
	next(error);   
});


app.use((error, req, res, next) => {  
	error.status(error.status || 500);
	
})

module.exports = app;