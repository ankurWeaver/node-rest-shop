const http = require('http');
const app = require('./app');

const port = 3600;

const server = http.createServer(app);

server.listen(port,function () {
	console.log("server started at "+port);
} );